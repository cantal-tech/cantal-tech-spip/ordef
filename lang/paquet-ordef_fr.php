<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ordef_description' => 'Permettre d\'ordonner/fusionner des valeurs candidates à la definition de _CONSTANTES php, selon l\'ordre d\'exécution des plugins. C\'est le <del>premier</del> dernier arrive qui décide.',
	'ordef_nom' => 'Constantes ordonnées',
	'ordef_slogan' => 'Un peu de souplesse dans la gestion des define()',
);
